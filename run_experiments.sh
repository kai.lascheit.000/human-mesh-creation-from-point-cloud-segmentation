#!/bin/bash

datasets=("PosePrior") #("egobody" "behave" "hi4d")

for dataset in "${datasets[@]}"; do
    # # no body parts
    # python src/main.py --dataset="$dataset" --use_body_parts=False --log_path="/data/no_parts_$dataset.log"
    # # no centroids
    # python src/main.py --dataset="$dataset" --use_centroids=False  --log_path="/data/no_centroids_$dataset.log"
    # # normal
    # python src/main.py --dataset="$dataset" --log_path="/data/normal_$dataset.log"
    # only centroids
    python src/main.py --dataset="$dataset" --use_full_scan=False --max_iter=20 --max_points=1000 --log_path="/data/only_centroids_$dataset.log"
done

echo "Done."
