# Human Mesh Creation from Point Cloud Segmentation

## Project
The aim of this project is to combine the outputs of a human-body part segmentation model with a human body model to refine the segmentation and create an accurate 3D representation of the detected humans.