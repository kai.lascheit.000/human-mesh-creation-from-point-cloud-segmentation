import os
import pdb
import numpy as np
import smplx
import open3d as o3d
import pickle
import torch

from body_part_idcs import SMPLX_2_BODY_PARTS

# with open(os.getcwd() + "/data/behave/ground_truth_params/_Date01_Sub01_backpack_back_t0011000_person_fit02_person_fit.pkl", "rb") as f:
#     seg = pickle.load(f)
#     pdb.set_trace()

num_scans_per_instance = 3
vertices_subset_size = 5000

base_dir = os.getcwd() + "/data/PosePrior/original/"
subjects = os.listdir(base_dir)
save_base = os.getcwd() + "/data/PosePrior/full_scans/"
ground_truth_base = os.getcwd() + "/data/PosePrior/ground_truth_params/"

segmentations = {}
for subject in subjects:
    subject_dir = os.path.join(base_dir, subject) + "/"
    runs = os.listdir(subject_dir)
    for run in runs:
        if run.startswith("neutral"):
            continue
        print("\nevaluating run", run)
        smplx_params = np.load(subject_dir + run)
        num_scans = smplx_params["pose_body"].shape[0]
        chosen_scan_idcs = np.random.choice(num_scans, num_scans_per_instance, replace=False)
        for idx in chosen_scan_idcs:
            scan_name = subject + "_" + run.replace(".npz", "") + "_" + str(idx)
            # create SMPL-X vertices
            gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=smplx_params["gender"].item(), use_face_contour=False, num_betas=smplx_params["num_betas"].item())
            gt_model.reset_params(betas=smplx_params["betas"], body_pose=smplx_params["pose_body"][idx, :], global_orient=smplx_params["root_orient"][idx, :], transl=smplx_params["trans"][idx, :])
            gt_model_output = gt_model(return_verts=True, return_full_pose=True)
            vertices = gt_model_output.vertices.detach().numpy().squeeze()
            # extract subset of vertices
            chosen_vertex_idcs = np.random.choice(vertices.shape[0], vertices_subset_size, replace=False)
            chosen_vertices = vertices[chosen_vertex_idcs, :]

            chosen_vertices = vertices

            body_parts = {"body_semseg" : SMPLX_2_BODY_PARTS[chosen_vertex_idcs].astype(np.uint8)}
            # extract ground truth parameters
            true_params = {}
            true_params["gender"] = "neutral"
            true_params["betas"] = smplx_params["betas"][np.newaxis, :]
            true_params["body_pose"] = smplx_params["pose_body"][idx, :][np.newaxis, :]
            true_params["global_orient"] = smplx_params["root_orient"][idx, :][np.newaxis, :]
            true_params["transl"] = smplx_params["trans"][idx, :][np.newaxis, :]
            # save
            # with open(ground_truth_base + scan_name + ".pkl", "wb") as f:
            #     pickle.dump(true_params, f)
            segmentations[scan_name] = body_parts
            point_cloud = o3d.geometry.PointCloud()
            point_cloud.points = o3d.utility.Vector3dVector(chosen_vertices)
            save_name = save_base + scan_name + ".ply"
            o3d.io.write_point_cloud(save_name, point_cloud)
            print("saved point cloud to", save_name)

# with open(os.getcwd() + "/data/PosePrior/PosePrior_segmentation.pkl", "wb") as f:
#     pickle.dump(segmentations, f)