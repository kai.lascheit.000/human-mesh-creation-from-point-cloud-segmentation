import os
import pdb
import numpy as np
import smplx
import open3d as o3d
import pickle
import torch

from body_part_idcs import SMPLX_2_BODY_PARTS

# with open(os.getcwd() + "/data/behave/ground_truth_params/_Date01_Sub01_backpack_back_t0011000_person_fit02_person_fit.pkl", "rb") as f:
#     seg = pickle.load(f)
#     pdb.set_trace()

num_scans_per_instance = 3
vertices_subset_size = 5000

original_dir = os.getcwd() + "/data/PosePrior/original/"
save_base = os.getcwd() + "/data/PosePrior/full_scans/"
ground_truth_base = os.getcwd() + "/data/PosePrior/ground_truth_params/"
subjects = os.listdir(ground_truth_base)

segmentations = {}
for subject in subjects:
    # find subject data
    run_path = original_dir + subject.split("_")[0] + "/" + "_".join([subject.split("_")[1], subject.split("_")[2]]) + ".npz"
    smplx_params = np.load(run_path)
    idx = int(subject.split("_")[-1].replace(".pkl", ""))
    # create SMPL-X vertices
    gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=smplx_params["gender"].item(), use_face_contour=False, num_betas=smplx_params["num_betas"].item())
    gt_model.reset_params(betas=smplx_params["betas"], body_pose=smplx_params["pose_body"][idx, :], global_orient=smplx_params["root_orient"][idx, :], transl=smplx_params["trans"][idx, :])
    gt_model_output = gt_model(return_verts=True, return_full_pose=True)
    vertices = gt_model_output.vertices.detach().numpy().squeeze()
    body_parts = {"body_semseg" : SMPLX_2_BODY_PARTS[[i for i in range(vertices.shape[0])]].astype(np.uint8)}
    # save
    scan_name = subject.replace(".pkl", "")
    segmentations[scan_name] = body_parts
    point_cloud = o3d.geometry.PointCloud()
    point_cloud.points = o3d.utility.Vector3dVector(vertices)
    save_name = save_base + scan_name + ".ply"
    o3d.io.write_point_cloud(save_name, point_cloud)
    print("saved point cloud to", save_name)

with open(os.getcwd() + "/data/PosePrior/PosePrior_full_scan_segmentation.pkl", "wb") as f:
    pickle.dump(segmentations, f)