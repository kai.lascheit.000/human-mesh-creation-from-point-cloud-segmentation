import pickle
from os import getcwd, listdir
import pdb
import smplx
from pytorch3d import transforms
from scipy.spatial import cKDTree
import open3d as o3d
import numpy as np
from pyviz3d import visualizer
from plyfile import PlyData
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, precision_recall_curve, auc

from body_part_idcs import SMPLX_2_BODY_PARTS, COLOR_MAP_PARTS, PART_NAMES
from utils import select_humans


DATASET = "egobody_finetuned"
MAX_POINTS = 10000
SAVE_VISUS = False

def extract_params(smplx_model):
    smplx_params = {}
    smplx_params["body_pose"] = smplx_model.body_pose
    smplx_params["global_orient"] = smplx_model.global_orient
    smplx_params["betas"] = smplx_model.betas
    smplx_params["transl"] = smplx_model.transl
    return smplx_params

def get_smplx_vertices(scan_path, gender):
    with open(scan_path, "rb") as f:
        smplx_params = pickle.load(f)
        if not type(smplx_params) == dict:
            smplx_params = extract_params(smplx_params)
    smplx_params["gender"] = gender
    if len(smplx_params["body_pose"].shape) > 2:
        smplx_params["body_pose"] = transforms.matrix_to_axis_angle(smplx_params["body_pose"]).view(-1, 63)
        smplx_params["global_orient"] = transforms.matrix_to_axis_angle(smplx_params["global_orient"]).unsqueeze(0)
    gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=smplx_params["gender"], use_face_contour=False, num_betas=smplx_params["betas"].shape[1])
    gt_model.reset_params(betas=smplx_params["betas"], body_pose=smplx_params["body_pose"], global_orient=smplx_params["global_orient"], transl=smplx_params["transl"])
    gt_model_output = gt_model(return_verts=True, return_full_pose=True)
    vertices = gt_model_output.vertices.detach().numpy().squeeze()
    return vertices

def get_segmentation(scan, gender, human3d_segmentations, recordings_path, true_vertices=None):
    # get corresponding human3d segmentation
    if DATASET == "hi4d":
        gender_idx = 2 if gender == "female" else 1
        scan_name = scan.split("_")[0] + "_" + scan.split("_")[1]
        human3d_segmentation = human3d_segmentations[scan_name][gender_idx]
        human3d_vertices = human3d_segmentation[:, :3]
        human3d_body_parts = human3d_segmentation[:, 3]
        # align coordinate system of original segmentation
        point_cloud = o3d.geometry.PointCloud()
        point_cloud.points = o3d.utility.Vector3dVector(human3d_vertices)
        point_offset = human3d_segmentations[scan_name]["offset"].copy()
        point_offset[1], point_offset[2] = -point_offset[2], point_offset[1]
        rotation_matrix = o3d.geometry.get_rotation_matrix_from_xyz([np.pi/2, 0, 0])
        point_cloud.rotate(rotation_matrix, center=(0, 0, 0))
        human3d_vertices = np.asarray(point_cloud.points)
        human3d_vertices += point_offset
    elif DATASET == "PosePrior":
        scan_name = scan.replace(".pkl", "")
        human3d_body_parts = human3d_segmentations[scan_name]["body_semseg"]  
        vertex_data = PlyData.read(recordings_path + scan_name + ".ply")["vertex"].data
        human3d_vertices = np.array([np.array([vertex["x"], vertex["y"], vertex["z"]]) for vertex in vertex_data])
    elif DATASET == "egobody":
        scan_name = scan.replace("frame", "scene_main")[:-8]
        segmentation = human3d_segmentations["egobody_test_EgoBodyTestWBodyPartTestCorrected_" + scan_name]["body_semseg"]
        vertex_data = PlyData.read(recordings_path + scan_name + ".ply")["vertex"].data
        all_vertices = np.array([np.array([vertex["x"], vertex["y"], vertex["z"]]) for vertex in vertex_data])
        segmentation_idcs = select_humans(segmentation, all_vertices, 1, 1000)
        human3d_vertices, human3d_body_parts = [], []
        for idx in segmentation_idcs:
            current_segmentation = segmentation[idx]
            human_idcs = np.where(current_segmentation != 0)[0]
            human3d_vertices.append(all_vertices[human_idcs])
            human3d_body_parts.append(current_segmentation[human_idcs])
    elif DATASET == "egobody_train":
        scan_name = scan.replace("_000.pkl", "")
        segmentation = human3d_segmentations["camera_wearer_" + scan_name]["body_semseg"]
        vertex_data = PlyData.read(recordings_path + scan_name.replace("frame", "scene_main") + ".ply")["vertex"].data
        all_vertices = np.array([np.array([vertex["x"], vertex["y"], vertex["z"]]) for vertex in vertex_data])
        segmentation_idcs = select_humans(segmentation, all_vertices, 1, 1000)
        human3d_vertices, human3d_body_parts = [], []
        for idx in segmentation_idcs:
            current_segmentation = segmentation[idx]
            human_idcs = np.where(current_segmentation != 0)[0]
            human3d_vertices.append(all_vertices[human_idcs])
            human3d_body_parts.append(current_segmentation[human_idcs])
    elif DATASET == "egobody_finetuned":
        scan_name = scan.replace("_frame_", "_scene_main_").replace("_000.pkl", "")
        segmentation = human3d_segmentations[scan_name]["body_semseg"]
        vertex_data = PlyData.read(recordings_path + scan_name + ".ply")["vertex"].data
        all_vertices = np.array([np.array([vertex["x"], vertex["y"], vertex["z"]]) for vertex in vertex_data])
        human_vertices = all_vertices[segmentation != 0]
        human_seg = segmentation[segmentation != 0]
        true_human_1 = cKDTree(true_vertices[0])
        true_human_2 = cKDTree(true_vertices[1])
        dists_1, idcs_1 = true_human_1.query(human_vertices, k=1)      
        dists_2, idcs_2 = true_human_2.query(human_vertices, k=1) 
        human_1_verts = human_vertices[dists_1 < dists_2]     
        human_2_verts = human_vertices[dists_1 >= dists_2]
        human_1_seg = human_seg[dists_1 < dists_2]     
        human_2_seg = human_seg[dists_1 >= dists_2]
        human3d_vertices = [human_1_verts, human_2_verts]
        human3d_body_parts = [human_1_seg, human_2_seg]
    else:
        scan_name = scan.replace("_fit02_", "_").replace("_fit", "").replace(".pkl", "")
        human3d_body_parts = human3d_segmentations[scan_name]["body_semseg"]
        vertex_data = PlyData.read(recordings_path + scan_name + ".ply")["vertex"].data
        human3d_vertices = np.array([np.array([vertex["x"], vertex["y"], vertex["z"]]) for vertex in vertex_data])
    return human3d_vertices, human3d_body_parts

def get_body_parts(mesh_vertices, pointcloud_vertices):
    nn_tree = cKDTree(mesh_vertices)
    nn_dists, nn_idcs = nn_tree.query(pointcloud_vertices, k=1)
    try:
        if max(nn_dists) > 0.1:
            print("WARNING: nearest neighbour distance was above threshold: max distance {d}".format(d=max(nn_dists)))
    except:
        pdb.set_trace()
    body_parts = SMPLX_2_BODY_PARTS[nn_idcs]
    return body_parts

def get_score(true_vertices, human3d_vertices, pred_body_parts, scan=""):
    true_body_parts = get_body_parts(true_vertices, human3d_vertices)
    # calculate accuracy
    accuracy = np.sum(true_body_parts == pred_body_parts) / len(true_body_parts)
    # calculate IoU
    num_classes = np.unique(true_body_parts).shape[0]
    confusion = confusion_matrix(true_body_parts, pred_body_parts, labels=np.arange(num_classes))
    IoUs = []
    for i in range(num_classes):
        true_positives = confusion[i, i]  # True positives for class i
        false_positives = confusion[:, i].sum() - true_positives  # False positives for class i
        false_negatives = confusion[i, :].sum() - true_positives  # False negatives for class i
        IoU = true_positives / (true_positives + false_positives + false_negatives)
        if not np.isnan(IoU):
            IoUs.append(IoU)
    mean_IoU = np.mean(IoUs)
    
    # calculate AP
    ap_scores = []    
    for i in range(num_classes):
        # Binarize the true labels and predicted labels for the current class
        y_true_bin = (true_body_parts == i).astype(int)
        y_pred_bin = (pred_body_parts == i).astype(int)        
        # Calculate precision and recall
        precision, recall, _ = precision_recall_curve(y_true_bin, y_pred_bin)        
        # Calculate Average Precision (AP) for the current class
        ap = auc(recall, precision)
        ap_scores.append(ap)    
    # Calculate mean Average Precision (mAP)
    mean_ap = np.mean(ap_scores)
    return (accuracy, mean_IoU, mean_ap)

def random_subsample(vertices, segmentation):
    if len(segmentation) > MAX_POINTS:
        random_indices = np.random.choice(len(segmentation), size=MAX_POINTS, replace=False)
        segmentation = segmentation[random_indices]
        vertices = vertices[random_indices, :]
    return vertices, segmentation

def sort_scans(human3d_vertices, true_vertices, fitted_vertices):
    human3d_means = [np.mean(vertices, axis=0) for vertices in human3d_vertices]
    true_means = [np.mean(vertices, axis=0) for vertices in true_vertices]
    fitted_means = [np.mean(vertices, axis=0) for vertices in fitted_vertices]
    if len(fitted_vertices) > 1:
        if np.linalg.norm(human3d_means[0] - true_means[0]) > np.linalg.norm(human3d_means[1] - true_means[0]):
            true_vertices[0], true_vertices[1] = true_vertices[1], true_vertices[0]
        if np.linalg.norm(human3d_means[0] - fitted_means[0]) > np.linalg.norm(human3d_means[1] - fitted_means[0]):
            fitted_vertices[0], fitted_vertices[1] = fitted_vertices[1], fitted_vertices[0]
    # else:
    #     if np.linalg.norm(human3d_means[0] - true_means[0]) < np.linalg.norm(human3d_means[0] - true_means[1]):
    #         true_means = [true_means[0]]
    #     else:
    #         true_means = [true_means[1]]
    return human3d_vertices, true_vertices, fitted_vertices

if __name__ == "__main__":
    # read original segmentation
    with open(getcwd() + "/data/" + DATASET + "/" + DATASET + "_segmentation.pkl", "rb") as f:
        human3d_segmentations = pickle.load(f)

    # compare human3d segmentation
    scans_path = getcwd() + "/data/" + DATASET + "/ground_truth_params/"
    meshes_path = getcwd() + "/data/" + DATASET + "/fitted_meshes/"
    recordings_path = getcwd() + "/data/" + DATASET + "/recordings/"
    scans = sorted(listdir(scans_path))
    if DATASET in ["egobody", "egobody_train", "egobody_finetuned"]:
        scans = [scan.replace("camera_wearer_", "") for scan in scans if scan.startswith("camera_wearer_")]
    human3d_scores = []
    mesh_scores = []
    for scan_idx, scan in enumerate(scans):
        if DATASET == "egobody_finetuned" and scan.replace("_frame_", "_scene_main_").replace("_000.pkl", "") not in human3d_segmentations.keys():
            print("could not find", scan)
            continue
        # read smplx vertices
        gender = scan.split("_")[-1].replace(".pkl", "") if DATASET == "hi4d" else "neutral"
        true_vertices = get_smplx_vertices(scans_path + scan, gender) if DATASET not in ["egobody", "egobody_train", "egobody_finetuned"] else [get_smplx_vertices(scans_path + "interactee_" + scan, gender), get_smplx_vertices(scans_path + "camera_wearer_" + scan, gender)]
        mesh_path = meshes_path + scan
        if DATASET == "behave":
            mesh_path = mesh_path.replace("_fit02_", "_").replace("_fit", "")
        if DATASET not in ["egobody", "egobody_train", "egobody_finetuned"]:
            try:
                with open(mesh_path, "rb") as f:
                    fitted_vertices = pickle.load(f)
            except FileNotFoundError:
                print("WARNING: could not find", mesh_path)
                continue
        elif DATASET in ["egobody", "egobody_train"]:
            fitted_vertices = []
            for subject in ["0", "1"]:
                try:
                    with open(mesh_path.replace("frame", "scene_main").replace("000", subject), "rb") as f:
                        fitted_vertices.append(pickle.load(f))
                except FileNotFoundError:
                    print("WARNING: could not find", mesh_path)
                    continue
            if len(fitted_vertices) == 1 and len(true_vertices) == 2:
                if np.linalg.norm(fitted_vertices[0].mean(axis=0) - true_vertices[0].mean(axis=0)) < np.linalg.norm(fitted_vertices[0].mean(axis=0) - true_vertices[1].mean(axis=0)):
                    true_vertices = [true_vertices[0]]
                else:
                    true_vertices = [true_vertices[1]]
        elif DATASET == "egobody_finetuned":
            fitted_vertices = [np.zeros_like(elt) for elt in true_vertices]

        # pdb.set_trace()
        # read segmentation from human3d
        try:
            human3d_vertices, human3d_body_parts = get_segmentation(scan, gender, human3d_segmentations, recordings_path, true_vertices)
        except KeyError:
            print("couldn't find segmentation for", scan)
            continue
        if DATASET not in ["egobody", "egobody_train", "egobody_finetuned"]:
            human3d_vertices, human3d_body_parts = random_subsample(human3d_vertices, human3d_body_parts) 
        else:
            for i in range(len(human3d_vertices)):
                human3d_vertices[i], human3d_body_parts[i] = random_subsample(human3d_vertices[i], human3d_body_parts[i])
        # sort egobody scans
        if DATASET in ["egobody", "egobody_train", "egobody_finetuned"]:
            human3d_vertices, true_vertices, fitted_vertices = sort_scans(human3d_vertices, true_vertices, fitted_vertices)
        else:
            human3d_vertices, true_vertices, fitted_vertices = [human3d_vertices], [true_vertices], [fitted_vertices]
            human3d_body_parts = [human3d_body_parts]
        # compute scores
        for i in range(len(human3d_vertices)):
            if human3d_vertices[i].shape[0] == 0:
                print("skipped scan", scan, "instance", i)
                continue
            fitted_body_parts = get_body_parts(fitted_vertices[i], human3d_vertices[i])
            h3d_accuracy, h3d_IoU, h3d_ap = get_score(true_vertices[i], human3d_vertices[i], human3d_body_parts[i], scan)
            mesh_accuracy, mesh_IoU, mesh_ap = get_score(true_vertices[i], human3d_vertices[i], fitted_body_parts, scan)
            if DATASET == "PosePrior" and h3d_accuracy < 0.8:
                break
            human3d_scores.append((h3d_accuracy, h3d_IoU, h3d_ap))
            mesh_scores.append((mesh_accuracy, mesh_IoU, mesh_ap))
            print("ORIGINAL ACCURACY:", round(100 * h3d_accuracy, 2), "%")
            print("FITTED ACCURACY:", round(100 * mesh_accuracy, 2), "%")
            print("ORIGINAL IoU:", round(100 * h3d_IoU, 2), "%")
            print("FITTED IoU:", round(100 * mesh_IoU, 2), "%")
            print("ORIGINAL mAP:", round(100 * h3d_ap, 2), "%")
            print("FITTED mAP:", round(100 * mesh_ap, 2), "%")
            if SAVE_VISUS:
                viz = visualizer.Visualizer()
                viz.add_points("Point Cloud", human3d_vertices[i], np.ones_like(human3d_vertices[i]) * np.array([0, 0, 255]), point_size=5)
                for body_part in set(human3d_body_parts[i]):
                    body_part_vertices = human3d_vertices[i][np.where(human3d_body_parts == body_part)]
                    body_part_colours = np.ones_like(body_part_vertices) * COLOR_MAP_PARTS[body_part]
                    body_part_name = PART_NAMES[body_part]
                    viz.add_points(body_part_name, body_part_vertices, body_part_colours, point_size=5)
                viz.add_points("Ground Truth", true_vertices[i], np.ones_like(true_vertices[i]) * np.array([0, 255, 0]), point_size=5)
                viz.add_points("Fitted Mesh", fitted_vertices[i], np.ones_like(fitted_vertices[i]) * np.array([255, 0, 0]), point_size=5)
                viz.save("viz/" + DATASET + "/" + scan.replace(".pkl", "") + "_" + str("i"))

    print("\nAVERAGE ORIGINAL ACCURACY:", round(100 * np.mean([elt[0] for elt in human3d_scores]), 2), "%")
    print("AVERAGE FITTED ACCURACY:", round(100 * np.mean([elt[0] for elt in mesh_scores]), 2), "%")

    print("AVERAGE ORIGINAL IoU:", round(100 * np.mean([elt[1] for elt in human3d_scores]), 2), "%")
    print("AVERAGE FITTED IoU:", round(100 * np.mean([elt[1] for elt in mesh_scores]), 2), "%")

    print("AVERAGE ORIGINAL mAP:", round(100 * np.mean([elt[2] for elt in human3d_scores]), 2), "%")
    print("AVERAGE FITTED mAP:", round(100 * np.mean([elt[2] for elt in mesh_scores]), 2), "%")

    pdb.set_trace()

    percentages = [round(100 * elt) for elt in human3d_scores]
    sorted_percentages = sorted(percentages)
    bins = np.arange(0, 101, 1)
    cumulative_counts = [sum(p <= b for p in sorted_percentages) for b in bins]
    plt.figure(figsize=(10, 6))
    plt.bar(bins, cumulative_counts, width=1, align='edge', edgecolor='black')

    plt.xlabel('Percentage')
    plt.ylabel('Cumulative Count')
    plt.title('Cumulative Histogram of Percentages')
    plt.grid(True)
    plt.show()