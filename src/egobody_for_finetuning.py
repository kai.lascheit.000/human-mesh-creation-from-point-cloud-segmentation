import os
import pdb
import pickle as pkl
import open3d as o3d
import numpy as np
from plyfile import PlyData, PlyElement
import pyrender

from visualisations import show_axes


base_dirs = ["../smplx_camera_wearer_train", "../smplx_interactee_train"]
recording = "recording_20210907_S02_S01_01"

for base_dir in base_dirs:
    sub_dir = os.listdir(os.path.join(base_dir, recording))[0]
    frames = [frame for frame in os.listdir(os.path.join(base_dir, recording, sub_dir, "results")) if frame + ".ply" in os.listdir(os.path.join("..", recording))]
    for frame in frames:
        # inputs to segfit
        with open(os.path.join(base_dir, recording, sub_dir, "results", frame, "000.pkl"), "rb") as f:
            data = pkl.load(f)
        pcd = o3d.io.read_point_cloud(os.path.join("..", recording, frame + ".ply"))            
        subject = "camera_wearer" if "camera_wearer" in base_dir else "interactee"
        with open("data/egobody_train/ground_truth_params/" + subject + "_" + recording + "_" + frame + "_000.pkl", "wb") as f:
            pkl.dump(data, f)
        o3d.io.write_point_cloud("data/egobody_train/recordings/" + recording + "_scene_main_" + frame.split("_")[-1] + ".ply", pcd)
        # inputs to human3d
        vertices = np.asarray(pcd.points)
        eval_input = np.array([(vertices[i, 0], vertices[i, 1], vertices[i, 2], 0, 0, 0, 0, 0) for i in range(vertices.shape[0])],
            dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'), ('blue', 'u1'), ('inst_label', 'u1'), ('label', 'u1')]
        )
        ply_element = PlyElement.describe(eval_input, 'vertex')
        ply_data = PlyData([ply_element])
        ply_data.write("data/egobody_train/human3d_inputs/" + subject + "_" + recording + "_" + frame + ".ply")

        # scene = pyrender.Scene()
        # mesh = pyrender.Mesh.from_points(vertices)
        # scene.add(mesh)
        # scene = show_axes(scene)
        # pyrender.Viewer(scene, use_raymond_lighting=True, point_size=2)