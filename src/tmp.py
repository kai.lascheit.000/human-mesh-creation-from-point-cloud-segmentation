import numpy as np
from os import getcwd, listdir
import pdb
import open3d as o3d
import torch
import smplx
import pickle as pkl
from body_part_idcs import COLOR_MAP_PARTS, map2color_parts, SMPLX_2_BODY_PARTS, colour2part
from pyviz3d import visualizer
import shutil
import os
import pyviz3d.visualizer as viz
from tqdm import tqdm
from plyfile import PlyData, PlyElement
from visualisations import show_axes, save_visu
import pyrender
from scipy.spatial import cKDTree
from sklearn.cluster import KMeans

from compare_segmentations import get_body_parts

# # data = np.load(getcwd() + "/data/cape/00145/shortlong_punching/shortlong_punching.000050.npz")
# # cano = data["v_cano"]
# # posed = data["v_posed"]

# # point_cloud = o3d.geometry.PointCloud()
# # point_cloud.points = o3d.utility.Vector3dVector(posed)
# # o3d.visualization.draw_geometries([point_cloud])

# # mesh = o3d.io.read_triangle_mesh(getcwd() + "/data/cape2/cape/scans/00032-longshort-hips-000150.obj")
# # print(mesh)
# # o3d.visualization.draw_geometries([mesh])

# # data = torch.load(getcwd() + "/data/cape2/cape_3views/00032-longshort-hips-000150/vis/120.pt")

# # joints = np.array([[-5.8022e-02,  2.6304e-01,  6.7338e-02],
# #          [-1.8429e-02,  1.6203e-01,  3.2492e-02],
# #          [-1.4989e-01,  2.0436e-01,  4.2701e-02],
# #          [-2.7933e-02,  3.6336e-01,  7.3429e-02],
# #          [ 1.3115e-01, -1.4913e-01,  1.5368e-01],
# #          [-2.2224e-01, -1.6113e-01,  5.9800e-02],
# #          [ 5.6338e-02,  4.4450e-01,  1.2624e-01],
# #          [ 1.7318e-01, -5.2680e-01,  8.6427e-02],
# #          [-1.9702e-01, -5.4610e-01,  7.1245e-02],
# #          [ 9.0780e-02,  4.5695e-01,  1.6874e-01],
# #          [ 2.1396e-01, -5.9458e-01,  1.8795e-01],
# #          [-2.1934e-01, -5.7502e-01,  1.9427e-01],
# #          [ 2.9519e-01,  5.0635e-01,  2.0072e-01],
# #          [ 2.2804e-01,  4.1663e-01,  1.6082e-01],
# #          [ 1.8932e-01,  5.5914e-01,  1.9145e-01],
# #          [ 3.4806e-01,  5.1651e-01,  2.6052e-01],
# #          [ 2.7951e-01,  3.4220e-01,  1.6655e-01],
# #          [ 2.0796e-01,  6.5007e-01,  1.7168e-01],
# #          [ 2.5668e-01,  9.1220e-02,  1.4513e-01],
# #          [ 1.7144e-01,  8.9232e-01,  1.4167e-01],
# #          [ 4.8220e-01,  7.2690e-02,  2.1929e-01],
# #          [ 3.6733e-01,  9.8423e-01,  2.5717e-01],
# #          [ 5.5512e-01,  1.0241e-01,  2.0034e-01],
# #          [ 4.3195e-01,  9.3961e-01,  2.3712e-01]])
# # blue_color = np.array([[0.0, 0.0, 1.0]] * len(joints))

# # vertices = np.loadtxt(getcwd() + '/../PTF/tensor_data.txt', delimiter=',')
# # gray_color = np.array([[0.5, 0.5, 0.5]] * len(vertices))

# # combined_points = np.vstack((vertices, joints))
# # combined_colors = np.vstack((gray_color, blue_color))

# # point_cloud = o3d.geometry.PointCloud()
# # point_cloud.points = o3d.utility.Vector3dVector(combined_points)
# # point_cloud.colors = o3d.utility.Vector3dVector(combined_colors)
# # o3d.visualization.draw_geometries([point_cloud])

# # smplx_params = np.load(getcwd() + "/data/PosePrior/03099/op3_stageii.npz", allow_pickle=True)
# # idx = 2000

# # gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=smplx_params["gender"].item(), use_face_contour=False, num_betas=smplx_params["num_betas"].item())
# # gt_model.reset_params(betas=smplx_params["betas"], body_pose=smplx_params["pose_body"][idx, :], global_orient=smplx_params["root_orient"][idx, :], transl=smplx_params["trans"][idx, :])
# # gt_model_output = gt_model(return_verts=True, return_full_pose=True)
# # vertices = gt_model_output.vertices.detach().numpy().squeeze()

# # point_cloud = o3d.geometry.PointCloud()
# # point_cloud.points = o3d.utility.Vector3dVector(vertices)
# # o3d.visualization.draw_geometries([point_cloud])

# # fname = "03099_lar3_stageii_1612"

# # with open(getcwd() + "/../demo_human3d_temp/PosePrior_segmentation.pkl", "rb") as f:
# #     segmentation = pickle.load(f)

# # print(set(segmentation[fname]["body_semseg"]))

# # pdb.set_trace()

# # pcd = o3d.io.read_point_cloud(getcwd() + "/../demo_human3d_temp/PosePrior_results/" + fname + ".ply")
# # o3d.visualization.draw_geometries([pcd])

# # with open(getcwd() + "/data/PosePrior/PosePrior_segmentation.pkl", "rb") as f:
# #     data = pickle.load(f)

# # recordings_dir = getcwd() + "/data/PosePrior/recordings/"
# # recordings = listdir(recordings_dir)
# # originals_dir = getcwd() + "/data/old_PosePrior/original/"
# # save_dir = getcwd() + "/data/PosePrior/ground_truth_params/"
# # with open(getcwd() + "/data/PosePrior/PosePrior_segmentation.pkl", "rb") as f:
# #     segmentations = pickle.load(f)

# # for recording in recordings:
# #     subject = recording.split("_")[0]
# #     run = "_".join([recording.split("_")[1], recording.split("_")[2]])
# #     idx = int(recording.split("_")[3].replace(".ply", ""))
# #     original_data = np.load(originals_dir + subject + "/" + run + ".npz")
# #     smplx_params = {}
# #     smplx_params["gender"] = "neutral"
# #     # num_betas = original_data["num_betas"].item()
# #     smplx_params["betas"] = original_data["betas"][np.newaxis, :]
# #     smplx_params["body_pose"] = original_data["pose_body"][idx][np.newaxis, :]
# #     smplx_params["global_orient"] = original_data["root_orient"][idx][np.newaxis, :]
# #     smplx_params["transl"] = original_data["trans"][idx][np.newaxis, :]

# #     pdb.set_trace()
# #     gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=smplx_params["gender"], use_face_contour=False, num_betas=smplx_params["betas"].shape[1])
# #     gt_model.reset_params(betas=smplx_params["betas"], body_pose=smplx_params["body_pose"], global_orient=smplx_params["global_orient"], transl=smplx_params["transl"])
# #     gt_model_output = gt_model(return_verts=True, return_full_pose=True)
# #     smplx_vertices = gt_model_output.vertices.detach().numpy().squeeze()
# #     smplx_colours = o3d.utility.Vector3dVector(np.ones_like(smplx_vertices) * np.array([0.0, 0, 0]))
    
# #     val_save_dir = getcwd() + "/../Human3D/data/raw/amass/validation/"
# #     pcd = o3d.io.read_point_cloud(val_save_dir + recording)
# #     scan_points = np.array(pcd.points)
# #     segmentation = segmentations[recording.replace(".ply", "")]["body_semseg"]
# #     colours = np.vstack(map2color_parts(segmentation)).T / 255.0
# #     scan_colours = o3d.utility.Vector3dVector(colours)


# #     # pcd = o3d.io.read_point_cloud(recordings_dir + recording)
# #     # scan_points = np.array(pcd.points)
# #     # scan_points[:, 2] = -scan_points[:, 2]
# #     # scan_points = scan_points[:, [0, 2, 1]]
# #     # scan_colours = o3d.utility.Vector3dVector(np.ones_like(scan_points) * np.array([0, 0, 1.0]))
        
# #     all_points = np.vstack([scan_points, smplx_vertices])
# #     all_colours = np.vstack([scan_colours, smplx_colours])
# #     pcd.points = o3d.utility.Vector3dVector(all_points)
# #     pcd.colors = o3d.utility.Vector3dVector(all_colours)
# #     o3d.visualization.draw_geometries([pcd])

# #     pdb.set_trace()


# #     with open(save_dir + recording.replace(".ply", ".pkl"), "wb") as f:
# #         pickle.dump(smplx_params, f)

# # recordings_dir = getcwd() + "/data/PosePrior/recordings_fucked/"
# # recordings = listdir(recordings_dir)
# # val_save_dir = getcwd() + "/../Human3D/data/raw/amass/validation/"
# # new_recordings_dir = getcwd() + "/data/PosePrior/recordings/"

# # for recording in recordings:
# #     pcd = o3d.io.read_point_cloud(val_save_dir + recording)
# #     o3d.io.write_point_cloud(new_recordings_dir + recording, pcd)

# # filtered_results = []
# # num_shit = 0
# # threshold = 80
# # human3d_scores = [96, 96, 96, 96, 95, 92, 96, 96, 95, 96, 93, 94, 87, 88, 90, 89, 89, 98, 97, 97, 88, 97, 85, 70, 69, 73, 97, 84, 35, 82, 71, 71, 97, 73, 71, 71, 70, 70, 72, 97, 97, 94, 96, 98, 62, 72, 74, 97, 96, 98, 87, 89, 89, 90, 89, 96, 95, 97, 90, 95, 95, 95, 91, 96, 95, 97, 96, 97, 86, 95, 96, 95, 97, 97, 97, 98, 97, 97, 97, 94, 88, 89, 94, 96, 95, 97, 97, 97, 97, 97, 97, 97, 92, 96, 96, 96, 97, 98, 96, 94, 93, 91, 92, 97, 97, 96, 96, 59, 96, 95, 98, 97, 82, 90, 84, 84, 97, 97, 81, 80, 81, 79, 78, 76, 76, 86, 92, 73, 68, 97, 68, 91, 64, 97, 96, 64, 66, 97, 87, 94, 90, 97, 96, 96, 95, 94, 91, 77, 66, 63]
# # with open(getcwd() + "/data/log_poseprior_concerning.log", "r") as f:
# #     results = f.readlines()
# #     score_idx = 0
# #     for line in results:
# #         if line.startswith("Mean Vertex Distance:"):
# #             result = float(line.split(" ")[3])
# #             if human3d_scores[score_idx] >= threshold: #result < 150:
# #                 filtered_results.append(result)
# #             else:
# #                 num_shit += 1
# #         elif line.startswith("Mean Joint Distance:"):
# #             result = float(line.split(" ")[3])
# #             if human3d_scores[score_idx] >= threshold: #result < 150:
# #                 filtered_results.append(result)
# #             score_idx += 1

# # assert score_idx == len(human3d_scores)
# # print("V2V:", np.mean([elt for i, elt in enumerate(filtered_results) if i%2 == 0]).round(1))
# # print("J2J:", np.mean([elt for i, elt in enumerate(filtered_results) if i%2 != 0]).round(1))
# # print(max(filtered_results))
# # print(round(100 * num_shit / len(human3d_scores), 2), "percent of scans were skipped")

# # pdb.set_trace()

# # percenatges = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 4, 6, 6, 8, 8, 10, 11, 14, 18, 20, 23, 24, 24, 26, 27, 28, 29, 30, 32, 34, 34, 37, 38, 40, 43, 46, 52, 57, 61, 65, 67, 74, 86, 110, 144, 150, 150, 150]


# # SAVE DATASET VISUS
# # save_folder = "paper_visus/"
# # if os.path.exists(save_folder):
# #     shutil.rmtree(save_folder)
# # os.makedirs(save_folder)
# # fpaths = {
#     # "egobody" : "data/egobody/recordings/recording_20210910_S05_S06_01_scene_main_01661.ply",
#     # "behave" : "data/behave/recordings/_Date01_Sub01_backpack_hug_t0015000_person_person.ply",
#     # "hi4d" : "data/hi4d/human3d_training_inputs/dance19_73.ply",
#     # "poseprior" : "data/PosePrior/recordings/03101_ulr1c_stageii_1122.ply"
# # }
# # for dataset, fpath in fpaths.items():
#     # pcd = o3d.io.read_point_cloud(fpath)
#     # points = np.asarray(pcd.points)
#     # points -= np.mean(points, axis=0)
#     # colours = np.asarray(pcd.colors) * 255
#     # viz = visualizer.Visualizer()
#     # viz.add_points("Point Cloud", points, colours, point_size=5)
#     # viz.save(save_folder + dataset)


# # with open("data/only_centroids_PosePrior.log") as f:
# #     results = f.readlines()
# #     best_j2js = []
# #     best_v2vs = []
# #     best_times = []
# #     iteration = 0
# #     for i, line in enumerate(results):
# #         if line.startswith("Iteration"):
# #             iteration = int(line.replace("Iteration ", "").replace(":", ""))
# #             valid_iteration = human3d_scores[iteration-1] >= 80
# #             min_j2j = np.inf
# #             min_v2v = np.inf
# #             full_scan_time = 0
# #             centroid_time = 0
# #             use_time = True
# #         elif line.startswith("Mean Joint Distance"):
# #             j2j = float(line.strip().replace("Mean Joint Distance: ", "").replace(" mm", ""))
# #             if j2j < min_j2j:
# #                 min_j2j = j2j
# #                 use_time = True
# #             else:
# #                 use_time = False
# #         elif line.startswith("Mean Vertex Distance"):
# #             v2v = float(line.strip().replace("Mean Vertex Distance: ", "").replace(" mm", ""))
# #             if v2v < min_v2v:
# #                 min_v2v = v2v
# #         elif line.startswith("Mean Centroid Fitting Time"):
# #             time = float(line.strip().replace("Mean Centroid Fitting Time: ", "").replace(" s", ""))
# #             if use_time:
# #                 centroid_time = time
# #             if i % 5 == 0 and valid_iteration:
# #                 best_j2js.append(min_j2j)
# #                 best_v2vs.append(min_v2v)
# #                 best_times.append(full_scan_time + centroid_time)
# #         elif line.startswith("Mean Scan Fitting Time"):
# #             time = float(line.strip().replace("Mean Scan Fitting Time: ", "").replace(" s", ""))
# #             if use_time:
# #                 full_scan_time = time
# #             # if i % 5 == 0 and valid_iteration:
# #             #     best_j2js.append(min_j2j)
# #             #     best_v2vs.append(min_v2v)
# #             #     best_times.append(full_scan_time + centroid_time)
# #         else:
# #             continue
# #     print(np.mean(best_v2vs).round(1))
# #     print(np.mean(best_j2js).round(1))
# #     print(np.mean(best_times).round(3))
# #     pdb.set_trace()


# # v = viz.Visualizer()
# # with open("data/behave/ground_truth_params/_Date01_Sub01_boxlong_hand_t0011000_person_fit02_person_fit.pkl", "rb") as f:
# #     smpl_params = pickle.load(f)
# #     scene = smpl_params["vertices"].detach().cpu().numpy().squeeze()
# #     body_parts = SMPLX_2_BODY_PARTS[np.arange(0, scene.shape[0])].astype(np.uint8)
# # pdb.set_trace()
# # point_positions = scene[:, 0:3] - np.mean(scene[:, 0:3], axis=0)
# # point_colors = np.vstack(map2color_parts(body_parts)).T # scene[:, 3:6]
# # # point_labels = scene[:, -1].astype(int)
# # # point_normals = scene[:, 6:9]
# # # point_semantic_colors = create_color_palette()[point_labels]
# # point_size = 35.0

# # v.add_points('RGB Color', point_positions, point_colors, point_size=point_size, visible=False)
# # # v.add_points('Semantics', point_positions, point_semantic_colors, point_normals, point_size=point_size)
# # # v.add_lines('Normals', point_positions, point_positions + point_normals/10.0, visible=True)

# # # When we added everything we need to the visualizer, we save it.
# # v.save('example_normals')


# # example = PlyData.read("data/hi4d/human3d_training_inputs/dance19_1.ply")
# # # pdb.set_trace()

# with open("data/hi4d/hi4d_segmentation.pkl", "rb") as f:
#     prior_segmentation = pickle.load(f)


# # for scene in tqdm(data.keys()):
# #     subjects = [elt for elt in data[scene] if elt != "offset"]
# #     offset = data[scene]["offset"]
# #     for subject in subjects:
# #         vertices = data[scene][subject][:, :3] + offset

# #         # scene = pyrender.Scene()
# #         # mesh = pyrender.Mesh.from_points(vertices)
# #         # scene.add(mesh)
# #         # scene = show_axes(scene)
# #         # pyrender.Viewer(scene, use_raymond_lighting=True, point_size=2)
# #         # pdb.set_trace()

# #         eval_input = np.array([(vertices[i, 0], vertices[i, 1], vertices[i, 2], 0, 0, 0, 0, 0) for i in range(vertices.shape[0])],
# #             dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'), ('blue', 'u1'), ('inst_label', 'u1'), ('label', 'u1')]
# #         )
# #         ply_element = PlyElement.describe(eval_input, 'vertex')
# #         ply_data = PlyData([ply_element])
# #         ply_data.write("data/hi4d_separated/human3d_inputs/" + scene + "_" + str(subject) + ".ply")

        
# # python infer_mhbps.py general.checkpoint='pretrained/FSK.ckpt'

# with open("../demo_human3d_temp/egobody_train_segmentation.pkl", "rb") as f:
#     seg_results = pickle.load(f)

# base_dir = "../demo_human3d_temp/egobody_train_results/"
# correct_segs = {}
# for fname in listdir(base_dir):
#     segmentation = PlyData.read(base_dir + fname)['vertex'].data
#     vertices = np.vstack([np.array(segmentation[coord]) for coord in ["x", "y", "z"]]).T #+ prior_segmentation["_".join(fname.split("_")[:-1])]["offset"]
#     vertices[:, [1, 2]] = vertices[:, [2, 1]]
#     vertices[:, 1] = -vertices[:, 1]
#     # # vertices[:, 2] = -vertices[:, 2]
#     # colours = np.vstack([np.array(segmentation[col]) for col in ["red", "green", "blue"]]).T
#     # parts = np.array([colour2part[tuple(row)] for row in colours])
#     parts = seg_results[fname.replace(".ply", "")]["body_semseg"]
#     instances = seg_results[fname.replace(".ply", "")]["instance_seg"]

#     seg1 = parts.copy()
#     if np.unique(instances).shape[0] == 3:
#         seg1[instances != 1] = 0
#         seg2 = parts.copy()
#         seg2[instances != 2] = 0
#         seg = np.vstack([seg1, seg2])
#     else:
#         seg = seg1[np.newaxis, :]

        

#     # pdb.set_trace()
#     # human_points = vertices[np.where(parts != 0)]
#     # kmeans = KMeans(n_clusters=2)
#     # labels = kmeans.fit_predict(human_points)

#     # entire_labels = -1 * np.ones(vertices.shape[0])
#     # entire_labels[parts != 0] = labels
    
#     # seg1 = parts.copy()
#     # seg1[entire_labels == 1] = 0

#     # seg2 = parts.copy()
#     # seg2[entire_labels == 0] = 0

#     # seg = np.vstack([seg1, seg2])
#     correct_segs[fname.replace(".ply", "")] = {"body_semseg" : seg}


#     # point_cloud = o3d.geometry.PointCloud()
#     # point_cloud.points = o3d.utility.Vector3dVector(vertices)
#     # # point_cloud.colors = o3d.utility.Vector3dVector(colours)
#     # o3d.visualization.draw_geometries([point_cloud])

#     # # gender = "male" if fname.replace(".ply", "").split("_")[-1] == "1" else "female"
#     # gt_fname = fname.replace(".ply", "_000.pkl")
#     # with open("data/egobody_train/ground_truth_params/" + gt_fname, "rb") as f:
#     #     gt_smplx = pickle.load(f)
#     # # pdb.set_trace()
#     # gt_model = smplx.create("../smplx/models/", model_type='smplx', ext="npz", gender=gt_smplx["gender"], use_face_contour=False, num_betas=gt_smplx["betas"].shape[1])
#     # gt_model.reset_params(betas=gt_smplx["betas"], body_pose=gt_smplx["body_pose"], global_orient=gt_smplx["global_orient"], transl=gt_smplx["transl"])
#     # gt_model_output = gt_model(return_verts=True, return_full_pose=True)
#     # smplx_vertices = gt_model_output.vertices.detach().cpu().numpy().squeeze()

#     if "camera_wearer" in fname:
#         point_cloud = o3d.geometry.PointCloud()
#         point_cloud.points = o3d.utility.Vector3dVector(vertices)
#         o3d.io.write_point_cloud("data/egobody_train/recordings/" + fname.replace("camera_wearer_", "").replace("frame", "scene_main"), point_cloud)


#     # human_vertices = 
    
#     # true_parts = get_body_parts(smplx_vertices, vertices)
    
#     # accuracy = (100 * np.sum(true_parts == parts) / len(true_parts)).round(1)
#     # print("Accuracy:", accuracy, "%")
#     # pdb.set_trace()

#     # seg_result = seg_results[fname.replace(".ply", "")]["body_semseg"]

#     # prior_vertices = prior_segmentation["_".join(fname.split("_")[:-1])][2][:, :3] + prior_segmentation["_".join(fname.split("_")[:-1])]["offset"]
#     # prior_vertices[:, [1, 2]] = prior_vertices[:, [2, 1]]
#     # prior_vertices[:, 1] = -prior_vertices[:, 1]
#     # prior_vertices 
#     # scene = pyrender.Scene()
#     # mesh = pyrender.Mesh.from_points(vertices, colors=colours)
#     # scene.add(mesh)
#     # mesh = pyrender.Mesh.from_points(smplx_vertices, colors=np.ones_like(smplx_vertices) * 0.5)
#     # scene.add(mesh)
#     # # mesh = pyrender.Mesh.from_points(prior_vertices, colors=np.ones_like(prior_vertices) * np.array([1, 0, 0]))
#     # # scene.add(mesh)
#     # scene = show_axes(scene)
#     # pyrender.Viewer(scene, use_raymond_lighting=True, point_size=2)
#     # save_visu("egobody_train_visu", vertices, smplx_vertices, None, pcd_colours=colours)
#     # pdb.set_trace()

# # with open("data/egobody_train/egobody_train_segmentation.pkl", "wb") as f:
# #     pickle.dump(correct_segs, f)

# # pdb.set_trace()

# ply_data = PlyData.read('data/hi4d_raw/recordings/dance19_4.ply')
# vertices = ply_data['vertex'].data

with open("data/egobody/egobody_wrong.pkl", "rb") as f:
    original = pkl.load(f)
    # recording = original["teaser_scan.ply"]["pcd"]
    # o3d.io.write_point_cloud("data/egobody/recordings/teaser_scan.ply", recording)
    segmentation = original["teaser_scan.ply"]["seg"]

recording = o3d.io.read_point_cloud("data/egobody/recordings/recording_20211004_S19_S06_03_scene_main_04171.ply")
kmeans = KMeans(n_clusters=2)

pdb.set_trace()

human_points = np.asarray(recording.points)[segmentation != 0]
kmeans.fit(human_points)
labels = kmeans.labels_ 
centroids = kmeans.cluster_centers_

final_seg = np.array([segmentation.copy(), segmentation.copy()])
final_seg[0, segmentation != 0] *= labels
final_seg[1, segmentation != 0] *= (labels + 1) % 2
pdb.set_trace()

with open("data/egobody/egobody_segmentation.pkl", "wb") as f:
    pkl.dump({"teaser_scan.ply" : {"body_semseg" : final_seg}}, f)

# pdb.set_trace()