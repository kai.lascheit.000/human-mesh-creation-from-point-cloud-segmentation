import open3d as o3d
from os import getcwd
import pdb
import pickle
import numpy as np
import pyrender
from scipy.spatial import cKDTree
from plyfile import PlyData, PlyElement

from body_part_idcs import SMPLX_2_BODY_PARTS, map2color_parts

def scan_to_ply(rec_type, num_files):
  base_path = getcwd() + "/" + rec_type + "/frames_vis/"
  for i in range(1, num_files+1):
      fpath = base_path + "mesh-f{:05d}".format(i)
      with open(fpath + ".pkl", 'rb') as f:
          data = pickle.load(f)
      vertices = data["vertices"]
      point_cloud = o3d.geometry.PointCloud()
      point_cloud.points = o3d.utility.Vector3dVector(vertices)
      point_cloud.colors = o3d.utility.Vector3dVector(0.5 * np.ones_like(vertices))
      point_cloud.estimate_normals()
      o3d.io.write_point_cloud(fpath.replace("frames_vis", "recordings") + ".ply", point_cloud)

def save_point_cloud_w_inst_label(points_3d, filename, binary=True, with_label=True, verbose=True):
  """Save an RGB point cloud as a PLY file.
  Args:
    points_3d: Nx6 matrix where points_3d[:, :3] are the XYZ coordinates and points_3d[:, 4:] are
        the RGB values. If Nx3 matrix, save all points with [128, 128, 128] (gray) color.
  """
  assert points_3d.ndim == 2
  if with_label:
    assert points_3d.shape[1] == 8
    python_types = (float, float, float, int, int, int, int, int)
    npy_types = [('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'),
                 ('blue', 'u1'), ('inst_label', 'u1'), ('label', 'u1')]
  else:
    if points_3d.shape[1] == 3:
      gray_concat = np.tile(np.array([128], dtype=np.uint8), (points_3d.shape[0], 3))
      points_3d = np.hstack((points_3d, gray_concat))
    assert points_3d.shape[1] == 6
    python_types = (float, float, float, int, int, int)
    npy_types = [('x', 'f4'), ('y', 'f4'), ('z', 'f4'), ('red', 'u1'), ('green', 'u1'),
                 ('blue', 'u1')]
  if binary is True:
    # Format into NumPy structured array
    vertices = []
    for row_idx in range(points_3d.shape[0]):
      cur_point = points_3d[row_idx]
      vertices.append(tuple(dtype(point) for dtype, point in zip(python_types, cur_point)))
    vertices_array = np.array(vertices, dtype=npy_types)
    el = PlyElement.describe(vertices_array, 'vertex')

    # Write
    PlyData([el]).write(filename)
  else:
    # PlyData([el], text=True).write(filename)
    with open(filename, 'w') as f:
      f.write('ply\n'
              'format ascii 1.0\n'
              'element vertex %d\n'
              'property float x\n'
              'property float y\n'
              'property float z\n'
              'property uchar red\n'
              'property uchar green\n'
              'property uchar blue\n'
              'property uchar alpha\n'
              'end_header\n' % points_3d.shape[0])
      for row_idx in range(points_3d.shape[0]):
        X, Y, Z, R, G, B = points_3d[row_idx]
        f.write('%f %f %f %d %d %d 0\n' % (X, Y, Z, R, G, B))
  if verbose is True:
    print('Saved point cloud to: %s' % filename)

def create_gt_segmentation(rec_type, num_files):
    for i in range(1, num_files+1):
        # read scan and ground truths
        file = "{:05d}".format(i)
        scan = o3d.io.read_point_cloud(getcwd() + "/" + rec_type + "/recordings/mesh-f" + file + ".ply")
        with open(getcwd() + "/" + rec_type + "/smplx/0" + file + "_male.pkl", "rb") as f:
            data = pickle.load(f)
            person_1 = data["vertices"].cpu().detach().numpy().squeeze()
        with open(getcwd() + "/" + rec_type + "/smplx/0" + file + "_female.pkl", "rb") as f:
            data = pickle.load(f)
            person_2 = data["vertices"].cpu().detach().numpy().squeeze()
        # create body part ground truths
        person_1_tree = cKDTree(person_1)
        person_2_tree = cKDTree(person_2)
        instances = np.zeros(len(scan.points), dtype=np.int8)
        body_parts = np.zeros(len(scan.points), dtype=np.int8)
        scan_points = np.asarray(scan.points)
        for j, point in enumerate(scan_points):
            distance1, idx1 = person_1_tree.query(point, k=1)
            distance2, idx2 = person_2_tree.query(point, k=1)
            if distance1 < distance2:
                instance = 1
                body_part = SMPLX_2_BODY_PARTS[idx1]
            else:
                instance = 2
                body_part = SMPLX_2_BODY_PARTS[idx2]
            instances[j] = instance
            body_parts[j] = body_part
        result = np.hstack([np.asarray(scan_points), np.zeros_like(np.asarray(scan_points)), instances[:, np.newaxis], body_parts[:, np.newaxis]])
        save_path = getcwd() + "/human3d_training_inputs/" + rec_type + "_" + str(i) + ".ply"
        save_point_cloud_w_inst_label(result, save_path, binary=True, with_label=True, verbose=True)

if __name__ == "__main__":
   rec_type = "sidehug32"
   num_files = 150
   scan_to_ply(rec_type, num_files)
   create_gt_segmentation(rec_type, num_files)

# point_idcs = np.where(result[:, 6] < 3)[0]
# scene = pyrender.Scene()
# colours = map2color_parts(body_parts[point_idcs])
# colours = np.vstack([colours[0], colours[1], colours[2]]).T / 255.0
# mesh = pyrender.Mesh.from_points(result[point_idcs, 0:3], colors=colours)
# scene.add(mesh)
# mesh = pyrender.Mesh.from_points(person_2, colors=[0.3, 0.3, 0.3, 0.3])
# scene.add(mesh)
# mesh = pyrender.Mesh.from_points(person_1, colors=[1.0, 0.0, 0.0, 0.3])
# scene.add(mesh)
# camera = pyrender.PerspectiveCamera(yfov=np.pi / 3.0)
# camera_pose = np.eye(4)
# camera_pose[:3, 3] = [0, 1.2, 2]
# camera_node = scene.add(camera, pose=camera_pose)
# pyrender.Viewer(scene, use_raymond_lighting=True, point_size=4)

"""
Steps for data preprocessing:
- convert SMPL npz files to ply using inspect_hi4d.py
- convert SMPL to SMPLX using the SMPLX repo
- convert scans from pkl to ply using the scan_to_ply function
- add segmentation using the create_gt_segmentation function
"""