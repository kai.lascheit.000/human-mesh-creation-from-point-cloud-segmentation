import numpy as np
import pdb


def get_mean_error(pred_values, true_values, max_idx=None):
    mean_sq_error = pow((pred_values.detach().numpy().squeeze()[:max_idx, :] - true_values[:max_idx, :]).squeeze(), 2).mean()
    return np.sqrt(mean_sq_error) * 1000 # convert to mm

def find_matching_gt(true_vertices, pred_vertices):
    distances = [pow((pred_vertices.detach().numpy().squeeze() - true_vertices[i, :]).squeeze(), 2).mean() for i in range(true_vertices.shape[0])]
    return np.argmin(distances)

def select_humans(segmentation, points, min_human_dist, min_points):
    lengths = np.zeros(segmentation.shape[0])
    means = []
    for i, instance in enumerate(segmentation):
        human_idcs = np.where(instance != 0)[0]
        means.append(points[human_idcs, :].mean(axis=0))
        lengths[i] = len(human_idcs)
    sorted_idcs = np.argsort(lengths)
    # check that returned point clouds belong to two different humans
    return_idcs = [sorted_idcs[-1]]
    for i in range(2, len(sorted_idcs)+1):
        sq_dist = np.sum(pow(means[sorted_idcs[-1]] - means[sorted_idcs[-i]], 2))
        if not np.isnan(sq_dist) and sq_dist > min_human_dist and lengths[sorted_idcs[-i]] > min_points:
            return_idcs.append(sorted_idcs[-i])
            break
    return return_idcs